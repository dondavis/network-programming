
# Communicating between the client and server using socket programming
In this lesson, we will learn how data from the server process is sent to the client process. This lesson is divided into the following parts:
```
Sending data to the client
Reading data that's been sent from the server
```
Before we begin with the main parts, let's quickly review the functions, structures, and terms that are used in successful client-server communication.


## Client-server model
Different models are used for IPC, but the most popular one is the client-server model. In this model, whenever the client needs some information, it connects to another process called the server. But before establishing the connection, the client needs to know whether the server already exists, and it should know the address of the server.

On the other hand, the server is meant to serve the needs of the client and does not need to know the address of the client prior to the connection. To establish a connection, a basic construct called a socket is required, and both the connecting processes must establish their own sockets. The client and the server need to follow certain procedures to establish their sockets.

To establish a socket on the client side, a socket is created with the socket function system call. Thereafter, that socket is connected to the server's address using the connect function system call, followed by sending and receiving data by invoking the read function and write function system calls.

To establish a socket on the server side, again, a socket is created with the socket function system call and then the socket is bonded to an address using the bind function system call. Thereafter, the listen function system call is invoked to listen for the connections. Finally, the connection is accepted by invoking the accept function system call.

struct sockaddr_in structure
This structure references the socket's elements that are used for keeping addresses. The following are the built-in members of this structure:
```c
struct sockaddr_in {
 short int sin_family;
 unsigned short int sin_port;
 struct in_addr sin_addr;
 unsigned char sin_zero[8];
};
```
Here, we have the following:
```
sin_family: Represents an address family. The valid options are AF_INET, AF_UNIX, AF_NS, and AF_IMPLINK. In most applications, the address family that's used is AF_INET.
sin_port: Represents the 16-bit service port number.
sin_addr: Represents a 32-bit IP address.
sin_zero: This is not used and is usually set to NULL.
struct in_addr comprise one member, as follows:
```
```c
struct in_addr {
     unsigned long s_addr; 
};
```
Here, s_addr is used to represent the address in network byte order.

socket()
This function creates an endpoint for communication. To establish communication, every process needs a socket at the end of the communication line. Also, the two communicating processes must have the same socket type and both should be in the same domain. Here is the syntax for creating a socket:
```c
int socket(int domain, int type, int protocol);
```
Here, domain represents the communication domain in which a socket is to be created. Basically, the address family or protocol family is specified, which will be used in the communication.

A few of the popular address family are listed as follows:                     
```
AF_LOCAL: This is used for local communication.
AF_INET: This is used for IPv4 internet protocols.
AF_INET6: This is used for IPv6 internet protocols.
AF_IPX: This is used for protocols that use standard IPX (short for Internetwork Packet Exchange) socket addressing. 
AF_PACKET: This is used for packet interface.
type: Represents the type of socket to be created. The following are the popular socket types:
SOCK_STREAM: Stream sockets communicate as a continuous stream of characters using a Transmission Control Protocol (TCP). TCP is a reliable stream-oriented protocol. So, the SOCK_STREAM type provides reliable, bidirectional, and connection-based byte streams.
SOCK_DGRAM: Datagram sockets read the entire messages at once using a User Datagram Protocol (UDP). UDP is an unreliable, connectionless, and message-oriented protocol. These messages are of a fixed maximum length.
SOCK_SEQPACKET: Provides reliable, bidirectional, and connection-based transmission paths for datagrams.
protocol: Represents the protocol to be used with the socket. A 0 value is specified so that you can use the default protocol that's suitable for the requested socket type.
```
You can replace the AF_ prefix in the preceding list with PF_ for protocol family.
On successful execution, the socket function returns a file descriptor that can be used to manage sockets.

#### memset()
This is used to fill a block of memory with the specified value. Here is its syntax:
```c
void *memset(void *ptr, int v, size_t n);
```
Here, ptr points at the memory address to be filled, v is the value to be filled in the memory block, and n is the number of bytes to be filled, starting at the location of the pointer.

### htons()
This is used to convert the unsigned short integer from host to network byte order.

### bind()
A socket that is created with the socket function remains in the assigned address family. To enable the socket to receive connections, an address needs to be assigned to it. The bind function assigns the address to the specified socket. Here is its syntax:
```c
int bind(int fdsock, const struct sockaddr *structaddr, socklen_t lenaddr);
```
Here, fdsock represents the file descriptor of the socket, structaddr represents the sockaddr structure that contains the address to be assigned to the socket, and lenaddr represents the size of the address structure that's pointed to by structaddr.

### listen()
It listens for connections on a socket in order to accept incoming connection requests. Here is its syntax:
```c
int listen(int sockfd, int lenque);
```
Here, sockfd represents the file descriptor of the socket, and lenque represents the maximum length of the queue of pending connections for the given socket. An error will be generated if the queue is full.

If the function is successful it returns zero, otherwise it returns -1.

### accept()
It accepts a new connection on the listening socket, that is, the first connection from the queue of pending connections is picked up. Actually, a new socket is created with the same socket type protocol and address family as the specified socket, and a new file descriptor is allocated for that socket. Here is its syntax:
```c
int accept(int socket, struct sockaddr *address, socklen_t *len);
```
Here, we need to address the following:
```
socket: Represents the file descriptor of the socket waiting for the new connection. This is the socket that is created when the socket function is bound to an address with the bind function, and has invoked the listen function successfully.
address: The address of the connecting socket is returned through this parameter. It is a pointer to a sockaddr structure, through which the address of the connecting socket is returned.
len: Represents the length of the supplied sockaddr structure. When returned, this parameter contains the length of the address returned in bytes.

```
### send()
This is used for sending the specified message to another socket. The socket needs to be in a connected state before you can invoke this function. Here is its syntax:
```c
       ssize_t send(int fdsock, const void *buf, size_t length, int flags);
```
Here, fdsock represents the file descriptor of the socket through which a message is to be sent, buf points to the buffer that contains the message to be sent, length represents the length of the message to be sent in bytes, and flags specifies the type of message to be sent. Usually, its value is kept at 0.

### connect()
This initiates a connection on a socket. Here is its syntax:
```c
int connect(int fdsock, const struct sockaddr *addr,  socklen_t len);
```
Here, fdsock represents the file descriptor of the socket onto which the connection is desired, addr represents the structure that contains the address of the socket, and len represents the size of the structure addr that contains the address.

### recv()
This is used to receive a message from the connected socket. The socket may be in connection mode or connectionless mode. Here is its syntax:
```c
ssize_t recv(int fdsock, void *buf, size_t len, int flags);
```
Here, fdsock represents the file descriptor of the socket from which the message has to be fetched, buf represents the buffer where the message that is received is stored, len specifies the length in bytes of the buffer that's pointed to by the buf argument, and flags specifies the type of message being received. Usually, its value is kept at 0.



























































