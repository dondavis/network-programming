
# Sending data to the client Example

* Define a variable of type sockaddr_in.
* Invoke the socket function to create a socket. The port number that's specified for the socket is 2000.
* Call the bind function to assign an IP address to it.
* Invoke the listen function.
* Invoke the accept function.
* Invoke the send function to send the message that was entered by the user to the socket.
* The socket at the client end will receive the message.

The server program, serverprog.c, for sending a message to the client is as follows:

```c

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>

int main(){
    int serverSocket, toSend;
    char str[255];
    struct sockaddr_in server_Address;
    serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    server_Address.sin_family = AF_INET;
    server_Address.sin_port = htons(2000);
    server_Address.sin_addr.s_addr = inet_addr("127.0.0.1");
    memset(server_Address.sin_zero, '\0', sizeof 
    server_Address.sin_zero); 
    bind(serverSocket, (struct sockaddr *) &server_Address, 
    sizeof(server_Address));
    if(listen(serverSocket,5)==-1)
    {
        printf("Not able to listen\n");
        return -1;
    }
    printf("Enter text to send to the client: ");
    gets(str);
    toSend = accept(serverSocket, (struct sockaddr *) NULL, NULL);
    send(toSend,str, strlen(str),0);
    return 0;
}

```
Let's use GCC to compile the serverprog.c program, as follows:
```
$ gcc serverprog.c -o serverprog
```
If you get no errors or warnings, this means that the serverprog.c program has compiled into an executable file, serverprog.exe. Let's run this executable file:
```
$ ./serverprog
Enter text to send to the client: thanks and good bye
```

## Reading Data sent from the server 

* Define a variable of type sockaddr_i.
* Invoke the socket function to create a socket. The port number that's specified for the socket is 2000.
* Invoke the connect function to initiate a connection to the socket.
* Because we are working on the local machine, the localhost address 127.0.0.1 is assigned to the socket.
* Invoke the recv function to receive the message from the connected socket. The message that's read from the socket is then displayed on the screen.

* The client program, clientprog.c, for reading a message that's sent from the server is as follows:

```c

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>

int main(){
    int clientSocket;
    char str[255];
    struct sockaddr_in client_Address;
    socklen_t address_size;
    clientSocket = socket(AF_INET, SOCK_STREAM, 0);
    client _Address.sin_family = AF_INET;
    client _Address.sin_port = htons(2000);
    client _Address.sin_addr.s_addr = inet_addr("127.0.0.1");
    memset(client _Address.sin_zero, '\0', sizeof client_Address.sin_zero); 
    address_size = sizeof server_Address;
    connect(clientSocket, (struct sockaddr *) &client_Address, address_size);
    recv(clientSocket, str, 255, 0);
    printf("Data received from server: %s", str);  
    return 0;
}

```

Here, let's use GCC to compile the clientprog.c program, as follows:
```
$ gcc clientprog.c -o clientprog
```
If you get no errors or warnings, this means that the clientprog.c program has compiled into an executable file, clientprog.exe. Let's run this executable file:
```
$ ./clientprog
Data received from server: thanks and good bye
```










