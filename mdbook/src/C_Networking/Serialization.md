
# Serialization

Serialization is a topic very closely related to data structure. Serialization involves converting from one data structure to a simpler data structure, usually some human-redable text or a raw binary sequence with some specific order or pattern. The reverse process, deserialization, involves converting the simpler data structure back into the original structure (or a similar one). Serialization is most often used when saving data to a file or sending it over a network.

Uses for serialization
Serialization allows the developer to save the state of an object and re-create it as needed, providing storage of objects as well as data exchange. Through serialization, a developer can perform actions such as:
```
Sending the object to a remote application by using a web service
Passing an object from one domain to another
Passing an object through a firewall as a JSON or XML string
Maintaining security or user-specific information across applications

```

# Sending Data to the Client

Define a variable of type sockaddr_in.
Invoke the socket function to create a socket. The port number that's specified for the socket is 2000.
Call the bind function to assign an IP address to it.
Invoke the listen function.
Invoke the accept function.
Invoke the send function to send the message that was entered by the user to the socket.
The socket at the client end will receive the message.
The server program, serverprog.c, for sending a message to the client is as follows:
```c
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>

int main(){
    int serverSocket, toSend;
    char str[255];
    struct sockaddr_in server_Address;
    serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    server_Address.sin_family = AF_INET;
    server_Address.sin_port = htons(2000);
    server_Address.sin_addr.s_addr = inet_addr("127.0.0.1");
    memset(server_Address.sin_zero, '\0', sizeof 
    server_Address.sin_zero); 
    bind(serverSocket, (struct sockaddr *) &server_Address, 
    sizeof(server_Address));
    if(listen(serverSocket,5)==-1)
    {
        printf("Not able to listen\n");
        return -1;
    }
    printf("Enter text to send to the client: ");
    gets(str);
    toSend = accept(serverSocket, (struct sockaddr *) NULL, NULL);
    send(toSend,str, strlen(str),0);
    return 0;
}
```
## Explanation

We will start by defining a string of size 255, and a server_Address variable of type sockaddr_in. This structure references the socket's elements. Then, we will invoke the socket function to create a socket by the name of serverSocket. A socket is an endpoint for communication. The address family that's supplied for the socket is AF_INET, and the socket type selected is the stream socket type, since the communication that we want is of a continuous stream of characters.

The address family that's specified for the socket is AF_INET, and is used for IPv4 internet protocols. The port number that's specified for the socket is 2000. Using the htons function, the short integer 2000 is converted into the network byte order before being applied as a port number. The fourth parameter, sin_zero, of the server_Address structure is set to NULL by invoking the memset function.

To enable the created serverSocket to receive connections, call the bind function to assign an address to it. Using the sin_addr member of the server_Address structure, a 32-bit IP address will be applied to the socket. Because we are working on the local machine, the localhost address 127.0.0.1 will be assigned to the socket. Now, the socket can receive the connections. We will invoke the listen function to enable the serverSocket to accept incoming connection requests. The maximum pending connections that the socket can have is 5.

You will be prompted to enter the text that is to be sent to the client. The text you enter will be assigned to the str string variable. By invoking the accept function, we will enable the serverSocket to accept a new connection.

The address of the connection socket will be returned through the structure of type sockaddr_in. The socket that is returned and that is ready to accept a connection is named toSend. We will invoke the send function to send the message that's entered by you. The socket at the client end will receive the message.

Let's use GCC to compile the serverprog.c program, as follows:
```
$ gcc serverprog.c -o serverprog
```
If you get no errors or warnings, this means that the serverprog.c program has compiled into an executable file, serverprog.exe. Let's run this executable file:
```
$ ./serverprog
```
```
Enter text to send to the client: thanks and good bye
```

## Reading Data from a server

Define a variable of type sockaddr_i.
Invoke the socket function to create a socket. The port number that's specified for the socket is 2000.
Invoke the connect function to initiate a connection to the socket.
Because we are working on the local machine, the localhost address 127.0.0.1 is assigned to the socket.
Invoke the recv function to receive the message from the connected socket. The message that's read from the socket is then displayed on the screen.
The client program, clientprog.c, for reading a message that's sent from the server is as follows:
```c
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>

int main(){
    int clientSocket;
    char str[255];
    struct sockaddr_in client_Address;
    socklen_t address_size;
    clientSocket = socket(AF_INET, SOCK_STREAM, 0);
    client _Address.sin_family = AF_INET;
    client _Address.sin_port = htons(2000);
    client _Address.sin_addr.s_addr = inet_addr("127.0.0.1");
    memset(client _Address.sin_zero, '\0', sizeof client_Address.sin_zero); 
    address_size = sizeof server_Address;
    connect(clientSocket, (struct sockaddr *) &client_Address, address_size);
    recv(clientSocket, str, 255, 0);
    printf("Data received from server: %s", str);  
    return 0;
}
```
## Explanation

So, we have defined a string of size 255 and a variable called client_Address of type sockaddr_in. We will invoke the socket function to create a socket by the name of clientSocket.

The address family that's supplied for the socket is AF_INET and is used for IPv4 internet protocols, and the socket type that's selected is stream socket type. The port number that's specified for the socket is 2000. By using the htons function, the short integer 2000 is converted into the network byte order before being applied as a port number.

We will set the fourth parameter, sin_zero, of the client_Address structure to NULL by invoking the memset function. We will initiate the connection to the clientSocket by invoking the connect function. By using the sin_addr member of the client_Address structure, a 32-bit IP address is applied to the socket. Because we are working on the local machine,  the localhost address 127.0.0.1 is assigned to the socket. Finally, we will invoke the recv function to receive the message from the connected clientSocket. The message that's read from the socket will be assigned to the str string variable, which will then be displayed on the screen.

Now, press Alt + F2 to open a second Terminal window. Here, let's use GCC to compile the clientprog.c program, as follows:                    
```
$ gcc clientprog.c -o clientprog
```
If you get no errors or warnings, this means that the clientprog.c program has compiled into an executable file, clientprog.exe. Let's run this executable file:
```
$ ./clientprog
```
```
Data received from server: thanks and good bye
```
Voila! We've successfully communicated between the client and server using socket programming.

